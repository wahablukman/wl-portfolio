import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { ReactComponent as HomepageIcon } from '../../assets/images/icons/home.svg';
import { ReactComponent as ExperienceIcon } from '../../assets/images/icons/experience.svg';
import { ReactComponent as WebsiteIcon } from '../../assets/images/icons/website.svg';
import { ReactComponent as PhotographyIcon } from '../../assets/images/icons/photography.svg';
import { ReactComponent as ContactIcon } from '../../assets/images/icons/contact.svg';
import BurgerMenu from '../../assets/images/icons/menu.svg';

import classnames from 'classnames';

class Navbar extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        <div
          onClick={this.props.onClickMenuToggle}
          className="tablet-only tablet-nav-toggle"
        >
          <img src={BurgerMenu} alt="" style={{ display: 'inline-block' }} />
          <p style={{ display: 'inline-block' }}>Menu</p>
        </div>
        <nav
          className={classnames({
            'menu-opened': this.props.menuOpened
          })}
        >
          <ul>
            <li>
              <NavLink onClick={this.props.onClickMenuToggle} exact to="/">
                <div className="icon">
                  <HomepageIcon />
                </div>
                <div className="text">Home</div>
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={this.props.onClickMenuToggle}
                exact
                to="/experiences"
              >
                <div className="icon">
                  <ExperienceIcon />
                </div>
                <div className="text">Experience</div>
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={this.props.onClickMenuToggle}
                exact
                to="/website"
              >
                <div className="icon">
                  <WebsiteIcon />
                </div>
                <div className="text">Website</div>
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={this.props.onClickMenuToggle}
                exact
                to="/photography"
              >
                <div className="icon">
                  <PhotographyIcon />
                </div>
                <div className="text">Photography</div>
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={this.props.onClickMenuToggle}
                exact
                to="/contact"
              >
                <div className="icon">
                  <ContactIcon />
                </div>
                <div className="text">Contact</div>
              </NavLink>
            </li>
          </ul>
        </nav>
      </React.Fragment>
    );
  }
}

export default Navbar;
