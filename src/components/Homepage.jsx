import React, { Component } from 'react';
import logo from '../assets/images/new-wl-logo2.svg';
// import Lottie from 'react-lottie';
import TypeIt from 'typeit';

class Homepage extends Component {
  state = {};

  componentDidMount() {
    new TypeIt('.typeit-homepage', {
      strings: [
        'A full-stack developer',
        'A hobbyist photographer',
        'Overall a web technology enthusiast',
      ],
      speed: 70,
      breakLines: false,
      autoStart: false,
    });
  }

  render() {
    // const animatedLogo = {
    //   loop: true,
    //   autoplay: true,
    //   animationData: require('../assets/images/animated-wl-logo.json'),
    //   rendererSettings: {
    //     preserveAspectRatio: 'xMidYMid slice'
    //   }
    // };
    return (
      <div className="container homepage-container">
        <section className="homepage-section">
          <img className="logo" src={logo} alt="Wahab Lukman's Logo" />
          {/* <Lottie
            options={animatedLogo}
            height={'auto'}
            width={300}
            isStopped={false}
            isPaused={false}
          /> */}
          <h2 className="heading-name">wahab lukman saad.</h2>
          <p className="typeit-homepage" />
        </section>
      </div>
    );
  }
}

export default Homepage;
