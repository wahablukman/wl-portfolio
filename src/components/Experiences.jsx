import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { ReactComponent as PrevButton } from '../assets/images/icons/prev-button.svg';
import { ReactComponent as NextButton } from '../assets/images/icons/next-button.svg';

class Experiences extends Component {
  state = {
    yearActive: 2018
  };

  onClickPrev = () => {
    if (this.state.yearActive === 2012) {
      this.setState({
        yearActive: 2018
      });
    } else {
      this.setState({
        yearActive: this.state.yearActive - 1
      });
    }
  };

  onClickNext = () => {
    if (this.state.yearActive === 2018) {
      this.setState({
        yearActive: 2012
      });
    } else {
      this.setState({
        yearActive: this.state.yearActive + 1
      });
    }
  };

  render() {
    const experiences = this.props.experiences.filter(
      experience => experience.year === this.state.yearActive
    );
    return (
      <div className="container experience-container vh-fix">
        <div className="year-active-button">
          <div className="prev-button" onClick={this.onClickNext}>
            <PrevButton />
          </div>
          <h2 className="number-fonts">{this.state.yearActive}</h2>
          <div className="next-button" onClick={this.onClickPrev}>
            <NextButton />
          </div>
        </div>
        <section className="experience-section">
          {experiences.map((experience, index) => {
            return (
              <div className="experience" key={index}>
                <div className="separator" />
                <p className="company">{experience.company}</p>
                <br />
                <p className="role">{experience.role}</p>
              </div>
            );
          })}
        </section>
      </div>
    );
  }
}

export default withRouter(Experiences);
