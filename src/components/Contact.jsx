import React, { Component } from 'react';
import axios from 'axios';
import keys from '../config/keys';
import Lottie from 'react-lottie';

class Contact extends Component {
  state = {
    _subject: '',
    _honey: '',
    _replyto: '',
    name: '',
    email: '',
    message: '',
    successMessage: '',
    errorMessage: '',
    loading: false
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    await this.setState({
      loading: true
    });
    let enquiryData = new FormData();
    enquiryData.append('_subject', `${this.state.name} sent an enquiry`);
    enquiryData.append('_replyto', this.state.email);
    enquiryData.append('name', this.state.name);
    enquiryData.append('email', this.state.email);
    enquiryData.append('message', this.state.message);

    if (this.state._honey !== '') {
      return this.setState(
        {
          errorMessage: 'Form not submitted, possible SPAM',
          loading: false
        },
        () => {
          setTimeout(() => {
            this.setState({
              errorMessage: ''
            });
          }, 5000);
        }
      );
    } else {
      try {
        const submittedForm = await axios.post(
          `https://formsapi.jabwn.com/key/${keys.WLPortfolioJABWNAPIKey}`,
          enquiryData
        );
        if (submittedForm.data.message === 'success') {
          this.setState(
            {
              loading: false,
              successMessage: `Enquiry submitted! We'll get back to you as soon as possible`,
              name: '',
              email: '',
              message: ''
            },
            () => {
              setTimeout(() => {
                this.setState({
                  successMessage: ''
                });
              }, 5000);
            }
          );
        } else {
          this.setState(
            {
              loading: false,
              errorMessage:
                'There is an error in sending your enquiry, try again in a bit'
            },
            () => {
              setTimeout(() => {
                this.setState({
                  errorMessage: ''
                });
              }, 5000);
            }
          );
        }
      } catch (err) {
        return this.setState(
          {
            loading: false,
            errorMessage: err
          },
          () => {
            setTimeout(() => {
              this.setState({
                errorMessage: ''
              });
            }, 5000);
          }
        );
      }
    }
  };

  render() {
    const animatedLoading = {
      loop: true,
      autoplay: true,
      animationData: require('../assets/images/loading.json'),
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };
    return (
      <div className="container contact-container">
        <section className="contact-section">
          <h1>Contact</h1>
          <p>
            Send me a message whether you want to discuss a project or web
            stuffs!
          </p>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                onChange={this.onChange}
                type="text"
                name="name"
                value={this.state.name}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                onChange={this.onChange}
                type="text"
                name="email"
                value={this.state.email}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="message">Message</label>
              <textarea
                onChange={this.onChange}
                rows="8"
                type="text"
                name="message"
                value={this.state.message}
                required
              />
            </div>
            <button type="submit">Send Message</button>
          </form>
          {this.state.loading ? (
            <div className="loading-spinner">
              <Lottie
                options={animatedLoading}
                height={'auto'}
                width={300}
                isStopped={false}
                isPaused={false}
              />
            </div>
          ) : null}
          {this.state.successMessage ? (
            <div className="success-message animated flipInX">
              <div className="success-text d-inline-block">
                {this.state.successMessage}
              </div>
            </div>
          ) : null}
          {this.state.errorMessage ? (
            <div className="error-message animated flipInX">
              <div className="error-text d-inline-block">
                {this.state.errorMessage}
              </div>
            </div>
          ) : null}
        </section>
      </div>
    );
  }
}

export default Contact;
