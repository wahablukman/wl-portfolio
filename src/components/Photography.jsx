import React, { Component } from 'react';
import axios from 'axios';
import Lottie from 'react-lottie';
import placeholderImage from '../assets/images/tiny-preview.jpg';
import ProgressiveImage from 'react-progressive-image';

class Photography extends Component {
  state = {
    photos: [],
    page: 1,
    pages: 0,
    loading: false
  };

  componentDidMount = () => {
    this.getFlickrPhotos();
  };

  getFlickrPhotos = async () => {
    await this.setState({
      loading: true
    });
    const flickrPhotos = await axios.get(
      `https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=b763c2b9a918047cb79e3a04f662102d&user_id=35402543%40N04&per_page=4&page=${
        this.state.page
      }&format=json&nojsoncallback=1`
    );

    // console.log(flickrPhotos);

    this.setState({
      pages: flickrPhotos.data.photos.pages,
      photos: flickrPhotos.data.photos.photo,
      loading: false
    });
  };

  onClickNext = async () => {
    await this.setState({
      loading: true
    });
    if (this.state.page === this.state.pages) {
      this.setState(
        {
          page: 1
        },
        () => {
          return this.getFlickrPhotos();
        }
      );
    } else {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          return this.getFlickrPhotos();
        }
      );
    }
  };

  onClickPrev = async () => {
    await this.setState({
      loading: true
    });
    if (this.state.page === 1) {
      this.setState(
        {
          page: this.state.pages
        },
        () => {
          return this.getFlickrPhotos();
        }
      );
    } else {
      this.setState(
        {
          page: this.state.page - 1
        },
        () => {
          return this.getFlickrPhotos();
        }
      );
    }
  };

  render() {
    const animatedLoading = {
      loop: true,
      autoplay: true,
      animationData: require('../assets/images/loading.json'),
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };
    return (
      <React.Fragment>
        <section className="photography-section vh-fix">
          {this.state.photos.map(photo => {
            return (
              <div className="photo" key={photo.id}>
                {/* <img
                  src={`https://farm${photo.farm}.staticflickr.com/${
                    photo.server
                  }/${photo.id}_${photo.secret}.jpg`}
                  alt={photo.title}
                /> */}
                <ProgressiveImage
                  src={`https://farm${photo.farm}.staticflickr.com/${
                    photo.server
                  }/${photo.id}_${photo.secret}.jpg`}
                  placeholder={placeholderImage}
                >
                  {src => <img src={src} alt={photo.title} />}
                </ProgressiveImage>
              </div>
            );
          })}
          <div className="slick-prev" onClick={this.onClickPrev} />
          <div className="slick-next" onClick={this.onClickNext} />
        </section>
        {this.state.loading ? (
          <div className="loading-spinner">
            <Lottie
              options={animatedLoading}
              height={'auto'}
              width={300}
              isStopped={false}
              isPaused={false}
            />
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

export default Photography;
