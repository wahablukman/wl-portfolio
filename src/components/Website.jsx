import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Slider from 'react-slick';

class Website extends Component {
  state = {};
  render() {
    var sliderSettings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false
    };
    return (
      <div className="container website-container">
        <section className="website-section">
          <Slider {...sliderSettings}>
            {this.props.webPortfolio.map((portfolio, index) => {
              return (
                <div className="portfolio" key={index}>
                  <div className="portfolio-image">
                    <img src={portfolio.portfolioImage} alt="" />
                  </div>
                  <div className="portfolio-details">
                    <h1 className="name">{portfolio.company}</h1>
                    <h3 className="year">{portfolio.year}</h3>
                    <p className="description">{portfolio.description}</p>
                  </div>
                </div>
              );
            })}
          </Slider>
        </section>
      </div>
    );
  }
}

export default withRouter(Website);
