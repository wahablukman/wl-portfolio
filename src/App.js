import React, { Component } from 'react';
import './assets/css/main.scss';
import { Route } from 'react-router-dom';
// import Particles from 'react-particles-js';

// loading layout components
import Navbar from './components/layout/Navbar';

// loading content components
import Homepage from './components/Homepage';
import Experiences from './components/Experiences';
import Website from './components/Website';
import Photography from './components/Photography';
import Contact from './components/Contact';

class App extends Component {
  state = {
    menuOpened: false,
    experiences: [
      {
        company: 'DeftByte',
        role: 'Full-stack JS Developer (MERN / MEVN Stack)',
        year: 2018,
        portfolio: true,
        description:
          'DeftByte consists of talented people ready to help business or individuals in developing their web / mobile / desktop application',
        portfolioImage: require('./assets/images/portfolios/DeftByteDesktop.png'),
      },
      {
        company: 'Dinamika Oscar Agung',
        role: 'Front-end Developer',
        year: 2018,
        portfolio: true,
        description:
          'PT. Dinamika Oscar Agung was in 2006 to provide solution for companies who demands a well known part from Japan. With the principle of high level service, the company aims to satisfy its customer from the first sales process after sales process as a one stop solution for its clients.',
        portfolioImage: require('./assets/images/portfolios/dinamikaoscaragung.png'),
      },
      {
        company: 'Artistik Salindia Lima',
        role: 'Front-end Developer',
        year: 2018,
        portfolio: true,
        description:
          'Artistik Salindia Lima is specialised in branding, design and marketing. Their services include graphic design, corporate identity, brand guidelines, packaging, print design and media',
        portfolioImage: require('./assets/images/portfolios/ASLDesktop.jpg'),
      },
      {
        company: 'Kembara Nusa',
        role: 'Front-end Developer',
        year: 2018,
        portfolio: true,
        description:
          'Kembara Nusa adalah yayasan yang mengajak para dokter gigi dan teman-teman relawan lain dari berbagai latar belakang pendidikan yang ingin membantu untuk meningkatkan kesehatan gigi dan mulut di wilayah Indonesia, sekaligus mengenalkan kekayaan alam Indonesia di daerah yang kami pilih sebagai project kami.',
        portfolioImage: require('./assets/images/portfolios/KembaraNusaDesktop.jpg'),
      },
      {
        company: 'Gignest',
        role: 'Full-stack Laravel / MySQL Developer',
        year: 2018,
        portfolio: true,
        description:
          'Gignest is an Indonesian talent showcase, providing people/ event organizer with selections of talents in the entertainment industry, including musician, stand-up comedian, magician, actor, sound engineer, and other relevant performers.',
        portfolioImage: require('./assets/images/portfolios/GignestDesktop.jpg'),
      },
      {
        company: 'Aaron Knight Freelancer',
        role: 'Web Support Developer',
        year: 2018,
        portfolio: false,
        description: '',
        portfolioImage: '',
      },
      {
        company: 'Sartorial Bay',
        role: 'Wordpress Support Developer',
        year: 2017,
        portfolio: true,
        description:
          'Nowadays in Sydney, Massimo (the founder), continues his tailoring legacy through Sartorial Bay by offering his expertise to transform Australian men’s image. Massimo’s goal with Sartorial Bay is to blend heritage tailoring with the coastal lifestyle, to create a unique new style that embraces formal and casual wearing in an organic symbiosis.',
        portfolioImage: require('./assets/images/portfolios/SartorialBayDesktop.jpg'),
      },
      {
        company: 'TunaKarya',
        role: 'Web Developer',
        year: 2017,
        portfolio: true,
        description:
          'TunaKarya adalah suatu komunitas berbasis forum yang menjadikan wadah bagi para “tunakarya” untuk berdiskusi, bercerita, memberikan info apapun yang berhubungan dengan tujuan utama mereka yaitu untuk mendapatkan pekerjaan yang layak.',
        portfolioImage: require('./assets/images/portfolios/TunakaryaDesktop.jpg'),
      },
      {
        company: 'L3BUNPAD 2017',
        role: 'Web Designer',
        year: 2017,
        portfolio: true,
        description:
          'Lomba Lintas Lembah dan Bukit (L3B) merupakan acara rutin Fakultas Pertanian Universitas Padjadjaran yang pertama kali dilaksanakan tahun 1962 di kampus Unpad Dago dengan memanfaatkan lembah serta bukit di sekitarnya, diketuai oleh mahasiswa angkatan kedua di Fakultas Pertanian Universitas Padjadjaran (Faperta angkatan 1960) yaitu Ir. Sugiat dengan Kang Djungdjung Hickman (Faperta Unpad 1959) sebagai penanggung jawab dari Seksi Olah Raga Senat KMFP.',
        portfolioImage: require('./assets/images/portfolios/l3bDesktop.jpg'),
      },
      {
        company: 'Zink and Sons',
        role: 'Wordpress Developer & Email Marketing',
        year: 2016,
        portfolio: true,
        description:
          'Zink and Sons has been dressing the gentlemen of Sydney since 1895 and have a reputation second to none, with Zink and Sons’ establishment now in its 6th generation under the ownership of Robert Jones and his son Daniel.',
        portfolioImage: require('./assets/images/portfolios/ZinkAndSonsDesktop.jpg'),
      },
      {
        company: 'Newtown Art Supplies',
        role: 'Web Designer & Email Marketing',
        year: 2016,
        portfolio: true,
        description:
          "Newtown Art Supplies is one of Australia's leading art supply stores selling accessories both online and from our art store just south of Sydney. Newtown is an inner city suburb of Sydney renowned for its off-beat and artistic culture.",
        portfolioImage: require('./assets/images/portfolios/NasDesktop.jpg'),
      },
      {
        company: 'Kadmium',
        role: 'Web Designer & Email Marketing',
        year: 2016,
        portfolio: true,
        description:
          "At Kadmium Art + Design supplies our aim is to combine specialty Art, Design and Architectural supplies with canvas stretching, in-house picture framing, imported stationery and unique gift ideas. Kadmium is much more than just your local art store, it's a hub for ideas, creativity and art classes as well as a platform for experiencing the latest art supplies technology.",
        portfolioImage: require('./assets/images/portfolios/KadmiumDesktop.jpg'),
      },
      {
        company: 'Connect You Here',
        role: 'Web Designer',
        year: 2015,
        portfolio: false,
        description: '',
        portfolioImage: '',
      },
      {
        company: 'L3BUNPAD',
        role: 'Web Designer',
        year: 2015,
        portfolio: false,
        description: '',
        portfolioImage: '',
      },
      {
        company: 'Soundquriang',
        role: 'Venue & Equipment Coordinator',
        year: 2014,
        portfolio: false,
        description: '',
        portfolioImage: ' ',
      },
      {
        company: 'DOCIT',
        role: 'Web Designer Internship',
        year: 2014,
        portfolio: false,
        description: '',
        portfolioImage: 'Resources/docit_presentation.png',
      },
      {
        company: 'PPIA Macquarie',
        role: 'Web Designer & Public Relation',
        year: 2013,
        portfolio: false,
        description: '',
        portfolioImage: '',
      },
      {
        company: 'United Way',
        role: 'Casual Graphic Designer and Photographer',
        year: 2013,
        portfolio: false,
        description: '',
        portfolioImage: ' ',
      },
      {
        company: 'Stereonesia',
        role: 'Coordinator of Trailer Committee',
        year: 2012,
        portfolio: false,
        description: '',
        portfolioImage: ' ',
      },
      {
        company: 'Indo Radio',
        role: 'Casual Photographer',
        year: 2012,
        portfolio: false,
        description: '',
        portfolioImage: ' ',
      },
    ],
  };

  componentDidMount() {}

  onClickMenuToggle = () => {
    this.setState({
      menuOpened: !this.state.menuOpened,
    });
  };

  onClickBodyMenuToggle = () => {
    if (this.state.menuOpened === true) {
      this.setState({
        menuOpened: !this.state.menuOpened,
      });
    }
  };

  render() {
    const webPortfolio = this.state.experiences.filter(
      (experience) => experience.portfolio
    );
    return (
      <div
        className="App vh-fix"
        onClick={this.onClickBodyMenuToggle}
        style={{ height: `${window.innerHeight}px` }}
      >
        <Navbar
          onClickMenuToggle={this.onClickMenuToggle}
          menuOpened={this.state.menuOpened}
        />
        {/* <Particles
          params={require('./assets/images/particlesjs.json')}
          className="particles"
        /> */}
        <div className="content">
          <Route exact path="/" component={Homepage} />
          <Route
            path="/experiences"
            component={() => (
              <Experiences experiences={this.state.experiences} />
            )}
          />
          <Route
            path="/website"
            component={() => <Website webPortfolio={webPortfolio} />}
          />
          <Route path="/photography" component={Photography} />
          <Route path="/contact" component={Contact} />
        </div>
      </div>
    );
  }
}

export default App;
